#!/bin/bash

ROOT_DIR=`pwd`
LICHEE_DIR=${ROOT_DIR}/lichee
UBOOT_DIR=${ROOT_DIR}/lichee/brandy/u-boot-2014.07
KERNEL_DIR=${ROOT_DIR}/lichee/linux-3.10
RESULT_DIR=${ROOT_DIR}/result
CPU_NUM=`grep -c processor /proc/cpuinfo`
BSP_NAME=
DATE=`date +%Y%m%d`

function build_uboot(){
	echo "=============== build uboot ======================"
	cd $ROOT_DIR
	if [ ! -L uboot ] && [ -d ./lichee/brandy/u-boot-2014.07 ]; then
		ln -s ./lichee/brandy/u-boot-2014.07 uboot
		echo "uboot link to ./lichee/brandy/u-boot-2014.07"
	elif [ ! -L uboot ] && [ ! -d ./lichee/brandy/u-boot-2014.07 ]; then
		echo "can not find uboot source code, failed to build uboot"
		exit 1
	fi

	cd ${ROOT_DIR}/lichee/brandy/u-boot-2014.07
	echo "=======path: `pwd`========="
	make distclean && make sun50iw6p1_config && make -j8	# compile uboot
	make distclean && make sun50iw6p1_config && make spl	# compile boot0
	
	if [ $? != 0 ]; then
		echo "=================== failed to build uboot ============================"
		exit 1
	fi
}

function build_kernel(){
	echo "=============== build kernel ======================"
	cd $ROOT_DIR
	if [ ! -L kernel ] && [ -d ./lichee/linux-3.10 ]; then
		ln -s ./lichee/linux-3.10 kernel
		echo "kernel link to ./lichee/linux-3.10"
	elif [ ! -L kernel ] && [ ! -d ./lichee/linux-3.10 ]; then
		echo "can not find kernel source code, failed to build kernel"
		exit 1
	fi
	
	cd ./lichee
#	echo "export LICHEE_TOOLCHAIN_PATH=${ROOT_DIR}/lichee/out/external-toolchain/gcc-aarch64"
	if [ ! -f def-buildconfig ]; then
		echo "export LICHEE_CHIP=sun50iw6p1" >> def-buildconfig
		echo "export LICHEE_PLATFORM=android" >> def-buildconfig
		echo "export LICHEE_BUSINESS=7.x" >> def-buildconfig
		echo "export LICHEE_ARCH=arm64" >> def-buildconfig
		echo "export LICHEE_KERN_VER=linux-3.10" >> def-buildconfig
		echo "export LICHEE_BOARD=" >> def-buildconfig
	#	echo "export LICHEE_BUSINESS=pad" >> def-buildconfig
	#	echo "export LICHEE_CROSS_COMPILER=aarch64-linux-gnu" >> def-buildconfig
	#	echo "export LICHEE_TOOLCHAIN_PATH=${ROOT_DIR}/lichee/out/external-toolchain/gcc-aarch64" >> def-buildconfig
	fi

	if [ ! -e .buildconfig ] ; then
		cp def-buildconfig .buildconfig
	fi
	./build.sh
	
	if [ $? != 0 ]; then
		echo "=================== failed to build kernel ============================"
		exit 1
	fi
}

function build_android(){
	echo "=============== build android  ======================"
	cd $ROOT_DIR
	source jdk_openJdk18.sh
	source build/envsetup.sh
	lunch petrel_fvd_p1-eng
	extract-bsp
	echo "================ cpu processor = ${CPU_NUM} ======================"
	make -j${CPU_NUM}
	
	if [ $? != 0 ]; then
		echo "=================== failed to build android ============================"
		exit 1
	fi
}

PACKAGE_NAME=h6-android7.0
function package_code(){
	cd $UBOOT_DIR
	make clean
	cd $KERNEL_DIR
	make clean

	cd $ROOT_DIR
	if [ -L uboot ] && [ -L kernel]; then
		rm uboot kernel
	fi
	cd $LICHEE_DIR
	rm .buildconfig def-buildconfig
	cd $ROOT_DIR/../
	tar czvf - ${PACKAGE_NAME} --exclude=${PACKAGE_NAME}/{result,out,lichee/out,lichee/tools/pack/out,lichee/tools/pack/*.img,lichee-backup-dual8-lvds-ok} | split -b 2000m -d -a 2 - ${PACKAGE_NAME}-${DATE}.tgz
#	tar cvf ${PACKAGE_NAME}-${DATE}.tar --exclude=${PACKAGE_NAME}/{result,out,lichee/out,lichee/tools/pack/out,lichee/tools/pack/sun50iw6p1_android_petrel-p1_uart0.img} ${PACKAGE_NAME}
	md5sum ${PACKAGE_NAME}-${DATE}.tgz* > md5sum.txt
	
}

function pack_image(){
	cd $ROOT_DIR
	pack > /dev/null 2>&1
	if [ $? != 0 ]; then
		source build/envsetup.sh
		lunch petrel_fvd_p1-eng
		pack
	fi	
}
function help_func(){
	echo "==================================================================================="
	echo "./make.sh uboot:			build uboot"
	echo "./make.sh kernel:			build kernel"
	echo "./make.sh android:		build android"
	echo "./make.sh all:			build all"
	echo "./make.sh clean:			clean uboot kernel"
	echo "./make.sh distclean:		distclean uboot kernel android"
	echo "./make.sh package:		package source code and generate md5 checksum" 
	echo "./make.sh pack:			pack image" 
	echo "./make.sh help:			this message" 
	echo "==================================================================================="

}

if [ $1 == "uboot" ]; then
	build_uboot;
	cd $ROOT_DIR
	source build/envsetup.sh
	lunch petrel_fvd_p1-eng
	pack
elif [ $1 == "kernel" ]; then
	build_kernel
	build_android
	pack
elif [ $1 == "android" ]; then
	build_android
	pack
elif [ $1 == "all" ]; then
	build_uboot
	build_kernel
	build_android
	pack
elif [ $1 == "package" ]; then
	package_code
elif [ $1 == "pack" ]; then
	pack_image
elif [ $1 == "clean" ]; then
	cd $UBOOT_DIR
	make clean
	cd $KERNEL_DIR
	make clean
elif [ $1 == "distclean" ]; then
	cd $ROOT_DIR
	rm uboot kernel
	cd $UBOOT_DIR
	make distclean
	cd $KERNEL_DIR
	make clean
	cd $LICHEE_DIR
	rm .buildconfig def-buildconfig
	cd $ROOT_DIR
	rm -rf out result
elif [ $1 == "help" ]; then
	help_func
else 
	echo "input wrong!!! usage:"
	help_func
fi

cd $ROOT_DIR
if [ ! -L result ]; then
	ln -s lichee/tools/pack result
fi


