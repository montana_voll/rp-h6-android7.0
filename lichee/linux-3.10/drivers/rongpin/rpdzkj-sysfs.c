//[*]--------------------------------------------------------------------------------------------------[*]
//
//
// 
//  rp4412 Board : rpdzkj sysfs driver (charles.park)
//  2012.01.17
// 
///sys/devices/platform/rpdzkj-sysfs/
//[*]--------------------------------------------------------------------------------------------------[*]
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/delay.h>
#include <linux/irq.h>
#include <linux/interrupt.h>
#include <linux/sysfs.h>
#include <linux/input.h>
#include <linux/gpio.h>
#include <linux/io.h>
#include <linux/wakelock.h>



static struct wake_lock rp_wake_lock;

static	int rpdzkj_sysfs_probe(struct platform_device *pdev)	
{
        				 
	printk("============ rpdzkj wake lock ban system enter deep sleep ============\n");
	wake_lock_init(&rp_wake_lock, WAKE_LOCK_SUSPEND, "rpdzkj_no_deep_sleep");
    wake_lock(&rp_wake_lock);
	

	return 0;
	
}

static int rpdzkj_sysfs_remove(struct platform_device *pdev)	
{
	
		
	wake_unlock(&rp_wake_lock);   	
	
    return	0;
}

static int rpdzkj_sysfs_resume(struct platform_device *dev)
{

    return  0;
}



static int rpdzkj_sysfs_suspend(struct platform_device *dev, pm_message_t state)
{

    return  0;
}

//[*]--------------------------------------------------------------------------------------------------[*]
static struct platform_driver rpdzkj_sysfs_driver = {
	
	.probe 		= rpdzkj_sysfs_probe,
	.remove 	= rpdzkj_sysfs_remove,
	.suspend	= rpdzkj_sysfs_suspend,
	.resume		= rpdzkj_sysfs_resume,
	.driver = {
		.name = "rpdzkj-sysfs",
		.owner = THIS_MODULE,
	},
};
static struct platform_device rpdzkj_device = {
	.name	= "rpdzkj-sysfs",
	.id		= -1,
};
static int __init rpdzkj_sysfs_init(void)
{	
	  int value=0;
	  platform_device_register(&rpdzkj_device);
   return platform_driver_register(&rpdzkj_sysfs_driver);
  
}

static void __exit rpdzkj_sysfs_exit(void)
{
    platform_driver_unregister(&rpdzkj_sysfs_driver);
    platform_device_unregister(&rpdzkj_device);
}

//late_initcall(rpdzkj_sysfs_init);
module_init(rpdzkj_sysfs_init);
module_exit(rpdzkj_sysfs_exit);

//[*]--------------------------------------------------------------------------------------------------[*]
MODULE_DESCRIPTION("SYSFS driver for rpdzkj board");
MODULE_AUTHOR("rpdzkj-kernel");
MODULE_LICENSE("GPL");

