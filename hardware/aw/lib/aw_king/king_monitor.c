#include "scene_policy.h"

enum GV_CPU_TEMPERATURE_THRESHOLD_STATE
{
	GV_CPU_TEMPERATURE_THRESHOLD_DEFAULT = -1,
	GV_CPU_TEMPERATURE_THRESHOLD_LOW_STATUS = 0,
	GV_CPU_TEMPERATURE_THRESHOLD_MID_STATUS = 1,
	GV_CPU_TEMPERATURE_THRESHOLD_HIGH_STATUS = 2,
	GV_CPU_TEMPERATURE_THRESHOLD_WARN_STATUS = 3,
};

static proc_sw_st proc_sw[] = {
	{GLORY_KING_POLICY, "/data/data/", "/data/data/com.tencent.tmgp.sgame", "cent.tmgp.sgame", 0, IN_CREATE, IN_OPEN, 0, NULL, 0, NULL},
};

static char *cpu_range_status[] = {
	"1800000 3 0 0 1800000 3 0 0",	// < 80
	"1488000 3 0 0 1488000 3 0 0",  // < 85
	"1320000 3 0 0 1320000 3 0 0",  // < 90
	"480000 3 0 0 1080000 3 0 0",  // < 95
};

static int cpu_threshold_value[] = {80, 85, 90, 95};

static int GV_CPU_TEMPERATURE_THRESHOLD_CUR_STATUS = GV_CPU_TEMPERATURE_THRESHOLD_DEFAULT;

int aw_get_cpu_temperature(int *temperature)
{
	int value = 0;
	char buf[16] = {0};

	if (aw_sysctrl_get(THS_CPU_CUR_TEMP, buf, sizeof(buf))) {
		return -1;
	}

	value = atoi(buf);

	if (value > 200 || value < (-200))
		return -1;

	*temperature = value;

	return 0;
}

int aw_get_meminfo(char *name, int length, int *value, char *units)
{
	int ret = -1;
	FILE *fp;
	char buf[100] = {0};
	fp = fopen(PROC_MEMINFO, "r");
	if (!fp) {
		B_ERR("%s: cant open, line=%d\n", __func__, __LINE__);
	}
	fseek(fp, 0, SEEK_SET);
	while (fgets(buf, 100, fp) != NULL) {
		if (!strncmp(buf, name, length)) {
			if ((sscanf(buf + length + 1, "%d %s", value, units)) == 2)
			{
				B_DEBUG("%s: %s = %d %s\n", __func__,name, *value, units);
				ret = 0;
				break;
			}
			memset(buf, 0, sizeof(buf));
			continue;
		}
	}
	fclose(fp);
	return ret;
}

int thermal_zone_onoff(int onoff)
{
	if (onoff == 1)
		aw_sysctrl_set(THS_CPU_ENABLE_SYS, "enabled");
	else if (onoff == 0)
		aw_sysctrl_set(THS_CPU_ENABLE_SYS, "disabled");
	else
		B_ERR("%s:wrong input onoff=%d\n", __func__, __LINE__);
	return 0;
}

int thermal_cpu_zone_onoff(void)
{
	int ret = -1;
	char buffer[16] = {0};
	ret = aw_sysctrl_get(THS_CPU_ENABLE_SYS, buffer, sizeof(buffer));
	if (ret) {
		return -1;
	}
	if (!strncmp(buffer, "enabled", 7)) {
		B_DEBUG("%s: enables\n", __func__);
		return 1;
	} else if (!strncmp(buffer, "disabled", 8)) {
		B_DEBUG("%s: enables\n", __func__);
		return 0;
	}
	return -1;
}

int thermal_cpu_cur_state(void)
{
	int ret = -1;
	int data = 0;
	char buf[16] = {0};

	ret = aw_sysctrl_get(THS_CPU_CUR_STATE, buf, sizeof(buf));
	if (ret) {
		B_ERR("%s: get falied\n", __func__);
		return -1;
	}

	data = atoi(buf);
	B_DEBUG("%s,state=%d\n", __func__, data);
	return data;
}

int cpu_roomage_state(char *value)
{
	int ret = -1;
	if (value == NULL)
	{
		B_ERR("%s: input value is null\n", __func__);
		return -1;
	}
	ret = aw_sysctrl_set(CPU_ROOMAGE_SYS, value);
	return ret;
}

int thermal_zone_cur_state(int state)
{
	char st[16] = {0};
	snprintf(st, sizeof(st), "%d", state);
	aw_sysctrl_set(THS_CPU_CUR_STATE, st);
	return 0;
}

int cpu_hotplug_onoff(int onoff)
{
	if (onoff == 1)
		aw_sysctrl_set(CPU_HOTPLUG_ENABLE_SYS, "1");
	else if (onoff == 0)
		aw_sysctrl_set(CPU_HOTPLUG_ENABLE_SYS, "0");
	else
		B_ERR("%s:wrong input onoff=%d\n", __func__, __LINE__);
	return 0;
}

int cpu_limit_freq(int max, int min)
{
	char max_buf[32] = {0};
	char min_buf[32] = {0};
	sprintf(max_buf, "%d", max);
	sprintf(min_buf, "%d", min);
	aw_sysctrl_set(CPU_MAXFREQ_SYS, max_buf);
	aw_sysctrl_set(CPU_MINFREQ_SYS, min_buf);
	return 0;
}

int cpu_online(int core, int onoff)
{
	char buf[128] = {0};
	sprintf(buf, "%s/cpu%d/online", CPU_CORE_PATH, core);
	if (onoff == 1)
		aw_sysctrl_set(buf, "1");
	else if (onoff == 0)
		aw_sysctrl_set(buf, "0");
	else
		B_ERR("%s:wrong input onoff=%d\n", __func__, __LINE__);
	return 0;
}

int gpu_perfmence_onoff(int onoff)
{
	if (onoff == 1)
		aw_sysctrl_set(GPU_SCENECTRL_SYS, "1");
	else if (onoff == 0)
		aw_sysctrl_set(GPU_SCENECTRL_SYS, "0");
	else
		B_ERR("%s:wrong input onoff=%d\n", __func__, __LINE__);
	return 0;
}

int gpu_irq_affinity(void)
{
	char buf[16] = "1";
	aw_sysctrl_set(GPU_IRQ_AFFINITY, buf);
	return 0;
}

int scene_policy(int pid, int onoff)
{
	B_ERR("%s: onoff=%d\n", __func__, onoff);
#if 0
	if (onoff == 1)
	{
		thermal_zone_onoff(0);
		cpu_hotplug_onoff(0);
		thermal_zone_cur_state(0);
		cpu_limit_freq(1800000, 1800000);
		cpu_online(0, 1);
		cpu_online(1, 1);
		cpu_online(2, 1);
		cpu_online(3, 0);
		gpu_irq_affinity();
		//gpu_perfmence_onoff(1);
		aw_king_monitor_threads(pid);
	} else {
		thermal_zone_onoff(1);
		cpu_hotplug_onoff(1);
		//gpu_perfmence_onoff(0);
		cpu_limit_freq(1800000, 480000);
		//ret = aw_king_monitor_threads(pid);
	}
#endif
	if (onoff == 1)
	{
		thermal_zone_onoff(0);
		thermal_zone_cur_state(0);
		cpu_roomage_state("1800000 3 0 0 1800000 3 0 0");
		gpu_irq_affinity();
		aw_king_monitor_threads(pid);
	} else {
		cpu_roomage_state("0 1 0 0 1800000 4 0 0");
		thermal_zone_onoff(1);
	}
	return 0;
}

int aw_set_policy_onoff(int onoff, proc_sw_st *proc_st)
{
	int ret;
	char policy[128];
	memset(policy, 0, sizeof(policy));
	switch (onoff) {
		case POLICY_ENABLE:
			snprintf(policy, sizeof(policy), "mode%d:1", proc_st->triger_pid);
			break;
		case POLICY_DISABLE:
			snprintf(policy, sizeof(policy), "mode%d:0", proc_st->triger_pid);
			break;
		default:
			return -1;
	}
	//ret = property_set(proc_st->scene_name, policy);
	ret = scene_policy(proc_st->triger_pid, onoff);
	return ret;
}

int aw_scene_watch(void *ptr)
{
	proc_sw_st *tmp = NULL;
	int temp_err = -1;
	int cur_temperature = 0;
	int retry = 2;
	int scene_policy = 0;
	int pid = -1;
	char comm[64];
	char oom_adj[32];
	if (!ptr) {
		B_ERR("%s: input ptr is null\n", __func__);
		return -1;
	}
	tmp = (proc_sw_st *)ptr;
	while (1) {
		/* check scene dir */
		if (aw_check_sysctrl_access(tmp->inotify_key, 1)) {
			/* watch /data/app */
			if (aw_inotify_event(tmp->inotify_item, tmp->item_event_mask)) {
				B_ERR("%s:no find inotify event\n", __func__);
				return -1;
			}
			continue;
		}
        snprintf(comm, sizeof(comm), "/proc/%d/comm", tmp->triger_pid);
		while (1) {
			if (aw_check_process(comm, tmp->triger_proc)) {
				/* watch /data/data/com.xxx.xxx */
				if (aw_inotify_event(tmp->inotify_key, tmp->key_event_mask)) {
					B_ERR("%s:no find inotify_key=%s\n", __func__, tmp->inotify_key);
					return -1;
				}
				retry = 2;
				while (retry--) {
					pid = aw_search_process(tmp->triger_proc);
					if (pid < 0)
						sleep(3);
				}
				if (pid < 0) {
					if(scene_policy) {
						/* target process exited , so disable scene policy */
						if (aw_set_policy_onoff(POLICY_DISABLE, tmp) == 0)
							scene_policy = 0;
					}
					continue;
				}
				tmp->triger_pid = pid;
				snprintf(comm, sizeof(comm), "/proc/%d/comm", tmp->triger_pid);
				snprintf(oom_adj, sizeof(oom_adj), "/proc/%d/oom_adj", tmp->triger_pid);
			}
			/* wait scene exit */
			while (!aw_check_process(comm, tmp->triger_proc)) {
				if (!aw_check_process(oom_adj, "0")) {
					/* game running */
					if (!scene_policy) {
						if (aw_set_policy_onoff(POLICY_ENABLE, tmp) == 0)
							scene_policy = 1;
					}

					/* policy has working */
					if (scene_policy) {
						/* cpu thermal status not equal zero,it means thermal
						 * irq had happened, it need to open thermal for 10
						 * seconds */
						B_DEBUG("GV_CPU_TEMPERATURE_THRESHOLD_CUR_STATUS=%d\n", GV_CPU_TEMPERATURE_THRESHOLD_CUR_STATUS);
						if (thermal_cpu_cur_state() != 0) {
							if (aw_set_policy_onoff(POLICY_DISABLE, tmp) == 0) {
								scene_policy = 0;
								GV_CPU_TEMPERATURE_THRESHOLD_CUR_STATUS = GV_CPU_TEMPERATURE_THRESHOLD_DEFAULT;
								sleep(10);
							}
						} else if ((temp_err == 0)
								&& (GV_CPU_TEMPERATURE_THRESHOLD_CUR_STATUS != GV_CPU_TEMPERATURE_THRESHOLD_LOW_STATUS)
								&& (cur_temperature < cpu_threshold_value[GV_CPU_TEMPERATURE_THRESHOLD_LOW_STATUS])) {

							B_DEBUG("set roomage status:%s\n", cpu_range_status[0]);
							GV_CPU_TEMPERATURE_THRESHOLD_CUR_STATUS = GV_CPU_TEMPERATURE_THRESHOLD_LOW_STATUS;
							cpu_roomage_state(cpu_range_status[0]);

						} else if ((temp_err == 0)
								&& (GV_CPU_TEMPERATURE_THRESHOLD_CUR_STATUS != GV_CPU_TEMPERATURE_THRESHOLD_MID_STATUS)
								&& (cur_temperature >= cpu_threshold_value[GV_CPU_TEMPERATURE_THRESHOLD_LOW_STATUS])
								&& (cur_temperature < cpu_threshold_value[GV_CPU_TEMPERATURE_THRESHOLD_MID_STATUS])) {

							B_DEBUG("set roomage status:%s\n", cpu_range_status[1]);
							GV_CPU_TEMPERATURE_THRESHOLD_CUR_STATUS = GV_CPU_TEMPERATURE_THRESHOLD_MID_STATUS;
							cpu_roomage_state(cpu_range_status[1]);

						} else if ((temp_err == 0)
								&& (GV_CPU_TEMPERATURE_THRESHOLD_CUR_STATUS != GV_CPU_TEMPERATURE_THRESHOLD_HIGH_STATUS)
								&& (cur_temperature >= cpu_threshold_value[GV_CPU_TEMPERATURE_THRESHOLD_MID_STATUS])
								&& (cur_temperature < cpu_threshold_value[GV_CPU_TEMPERATURE_THRESHOLD_HIGH_STATUS])) {

							B_DEBUG("set roomage status:%s\n", cpu_range_status[2]);
							GV_CPU_TEMPERATURE_THRESHOLD_CUR_STATUS = GV_CPU_TEMPERATURE_THRESHOLD_HIGH_STATUS;
							cpu_roomage_state(cpu_range_status[2]);

						} else if ((temp_err == 0)
								&& (GV_CPU_TEMPERATURE_THRESHOLD_CUR_STATUS != GV_CPU_TEMPERATURE_THRESHOLD_WARN_STATUS)
								&& (cur_temperature >= cpu_threshold_value[GV_CPU_TEMPERATURE_THRESHOLD_HIGH_STATUS])
								&& (cur_temperature < cpu_threshold_value[GV_CPU_TEMPERATURE_THRESHOLD_WARN_STATUS])) {

							B_DEBUG("set roomage status:%s\n", cpu_range_status[3]);
							GV_CPU_TEMPERATURE_THRESHOLD_CUR_STATUS = GV_CPU_TEMPERATURE_THRESHOLD_WARN_STATUS;
							cpu_roomage_state(cpu_range_status[3]);
						}
					}
					temp_err = aw_get_cpu_temperature(&cur_temperature);
					sleep(1);
				} else {
					if (scene_policy) {
						if (aw_set_policy_onoff(POLICY_DISABLE, tmp) == 0)
							scene_policy = 0;
					}
					sleep(5);
				}
			}
			if (scene_policy) {  /* target process finaly exited , so disable scene policy */
				if (aw_set_policy_onoff(POLICY_DISABLE, tmp) == 0)
					scene_policy = 0;
			}
		}
	}
	return 0;
}

int aw_mem_check(void)
{
	int ret = -1;
	char memtotal[] = "MemTotal";
	int mem_size = 0;
	int mem_size_M = 0;
	char units[8] = {0};
	ret = aw_get_meminfo(memtotal, sizeof(memtotal) - 1, &mem_size, units);
	if (!ret) {
		B_ERR("%s: memtotal = %d %s\n",__func__,  mem_size, units);
		mem_size_M = (mem_size/1024/512 + 1);
		/* mem must large than 2G */
		if (mem_size_M >= 4) {
			return 0;
		}
	}
	return -1;
}

int main(int argc, char **argv)
{
	int ret = 0;
	if (argc > 1 && argv[1]) {
		if (!strncmp("-d", argv[1], strlen("-d"))) {
			SCENE_POLICY_DEBUG_LEVEL = 2;
		}
	}
	if (aw_mem_check()) {
		B_ERR("mem size is smaller than 2G, eixt\n");
		return 0;
	}
	aw_scene_watch((void *)&proc_sw[0]);
	return 0;
}


