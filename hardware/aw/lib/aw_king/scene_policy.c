#include "scene_policy.h"


typedef int (*operate_callback)(int pid, void *buffer);


int aw_sysctrl_get(const char *path, char *buf, int slen)
{
	int fd;
	int numread;

	fd = open(path, O_RDONLY);
	if (fd == -1)
	{
		B_ERR("open fail,%s!\n",strerror(errno));
		goto err_out;
	}
	numread = read(fd, buf, slen - 1);
	if (numread < 1) {
		B_ERR("read fail,%s!\n",strerror(errno));
		close(fd);
		goto err_out;
	}

	buf[numread] = '\0';
	close(fd);
	B_INF(" get %s %s seccess!\n", path, buf);
	return 0;
err_out:
	return -1;
}

int aw_sysctrl_set(const char *path, const char *buf)
{
	int fd;
	int numwrite;

	fd = open(path, O_WRONLY);
	if (fd == -1)
	{
		B_ERR("open %s fail,%s!\n",path, strerror(errno));
		goto err_out;
	}
	numwrite = write(fd, buf, strlen(buf));
	if (numwrite < 1)
	{
		B_ERR("write %s fail,%s!\n",path,strerror(errno));
		close(fd);
		goto err_out;
	}
	close(fd);
	B_INF(" set %s %s seccess!\n", path, buf);
	return 0;
err_out:
	return -1;
}

int aw_devctrl_set(const char *path, int data)
{
	int fd;
	int numwrite;

	fd = open(path, O_WRONLY);
	if (fd == -1)
	{
		B_ERR("open %s fail,%s!\n",path, strerror(errno));
		goto err_out;
	}
	numwrite = write(fd, &data, sizeof(int));
	if (numwrite < 1)
	{
		B_ERR("read  %s fail,%s!\n",path,strerror(errno));
		close(fd);
		goto err_out;
	}
	close(fd);
	B_INF(" set %s %d seccess!\n", path, data);
	return 0;
err_out:
	return -1;
}

int aw_devctrl_get(const char *path, int *data)
{
	int fd;
	int numread;

	fd = open(path, O_RDONLY);
	if (fd == -1)
	{
		B_ERR("open fail,%s!\n",strerror(errno));
		goto err_out;
	}
	numread = read(fd, data,  sizeof(int));
	if (numread < 1) {
		B_ERR("read fail,%s!\n",strerror(errno));
		close(fd);
		goto err_out;
	}
	close(fd);
	B_INF(" get %s %d seccess!\n", path, *data);
	return 0;
err_out:
	return -1;
}

int aw_set_roomage(const char *path, char *buf)
{
	char tmp[128];
	int i;
	int fp;
	for (i = 20; i > 0; i--) {
		snprintf(tmp, sizeof(tmp), path, i);
		fp = open(tmp, O_RDONLY);
		if (fp < 0)
			continue;
		close(fp);
		aw_sysctrl_set(tmp, buf);
		return 0;
	}
	return -1;
}

int aw_get_magic(char magic, int magic_value,  plat_hw_slot *slot)
{
	int ret = -1;
	switch (magic)
	{
		case CPU_MAGIC:
			ret = CPU_MAGIC_SLOT;
		break;
		case GPU_MAGIC:
			ret = GPU_MAGIC_SLOT;
		break;
		case THERMAL_MAGIC:
			ret = THERMAL_MAGIC_SLOT;
		break;
		case MM_MAGIC:
			ret = MM_MAGIC_SLOT;
		break;
		case IDLE_MAGIC:
			ret = IDLE_MAGIC_SLOT;
		break;
		case EXT_MAGIC:
			ret = EXT_MAGIC_SLOT;
		break;
		case PRIO_MAGIC:
			ret = PRIO_MAGIC_SLOT;
		break;
		default:
			B_INF("unknow magic %c\n", magic);
			goto out;
		break;
	}
	slot->slot_active[ret] = 1;
	slot->slot_value[ret] = magic_value;
	B_INF("get magic:%c value:%d \n", magic, magic_value);
out:
	return ret;
}
/*
aw_get_para user guid
1. boost just support  mode{x}{$PID:x}{c:x}{g:x}{t:x}{m:x}
   mode{x} : x you can set any magic character you want
   {$PID:x}: pid is the major process pid ,x is the numbers of platform item
   {c:x}   : c is the cpu item of platform, x is the range you seltect
   {g:x}   : g is the gpu item of platform, x is the range you seltect
   {t:x}   : t is the thermal item of platform, x is the range you seltect
   {m:x}   : m is the memory item of platform, x is the range you seltect

   for example process 1009 want to control cpu and gpu to special mode, it can set
   : mode1009:4:c:1:g:1
   for example process 2880 want to control all item to special mode, it can set
   : mode2880:4:c:1:g:1:t:3:m:2
*/
int aw_get_para(const char *value, plat_hw_slot *hw_slot)
{
	/* moden1009:3:c:1:g:1:t:0:m:0 */
	char para_magic[8] = { 0 };
	int mpid, slot = 0;
	int ret = 0;
	char magic_a, magic_b, magic_c, magic_d, magic_e;
	int value_a, value_b, value_c, value_d, value_e;

	magic_a = magic_b = magic_c = magic_d = 0;
	value_a = value_b = value_c = value_d = 0;
	ret = sscanf(value, "%[a-z]%d:%d:%c:%d:%c:%d:%c:%d:%c:%d:%c:%d",
					para_magic,
					&mpid, &slot,
					&magic_a, &value_a,
					&magic_b, &value_b,
					&magic_c, &value_c,
					&magic_d, &value_d,
					&magic_e, &value_e);
	if (ret < 0)
	{
		B_ERR("prop value err:%d %s!\n", ret, strerror(errno));
		goto err_out;
	}
	B_INF("magic=%s, pid=%d, slot=%d ret=%d, %c=%d , %c=%d, %c=%d, %c=%d %c=%d\n",
		para_magic, mpid, slot, ret, magic_a, value_a, magic_b, value_b,
		magic_c, value_c, magic_d, value_d, magic_e, value_e);
	if (ret > 0)
	{
		hw_slot->pid = mpid;
		hw_slot->slot_used = slot;
		if (magic_a)
			aw_get_magic(magic_a, value_a, hw_slot);
		if (magic_b)
			aw_get_magic(magic_b, value_b, hw_slot);
		if (magic_c)
			aw_get_magic(magic_c, value_c, hw_slot);
		if (magic_d)
			aw_get_magic(magic_d, value_d, hw_slot);
		if (magic_e)
			aw_get_magic(magic_e, value_e, hw_slot);
	}
	return 0;
err_out:
	 return -1;
}

int aw_check_sysctrl_access(const char *path, int dentry)
{
	DIR *dir;
	int fd;

	if (!path) {
		B_ERR("%s:input path=%s is null\n", __func__, path);
		return -1;
	}

	if (dentry) {
		dir = opendir(path);
		if (!dir) {
			B_ERR("%s:cant open %s\n", __func__, path);
			return -1;
		}

		B_INF("%s:open %s success\n", __func__, path);
		closedir(dir);
		return 0;	
	}
	
	fd = open(path, O_RDONLY);
	if (fd < 0) {
		B_ERR("%s:cant open %s\n", __func__, path);
		return -1;
	}

	B_INF("%s:open %s success\n", __func__, path);
	close(fd);
	return 0;
}

int aw_taskset_threads(int pid, void *mask)
{
	int cpu_mask = *((int *)mask);
	return aw_taskset(pid, cpu_mask);
}

int aw_search_threads(int pid, operate_callback cb, void *buffer)
{
	DIR *task_dir = NULL;
	struct dirent *tid_dir = NULL;
	char file[64] = {0};
	int tid = 0;
	int ret = -1;

	sprintf(file, "proc/%d/task", pid);
	task_dir = opendir(file);
	if (task_dir) {
		while ((tid_dir = readdir(task_dir))) {
			if (!isdigit(tid_dir->d_name[0]))
				continue;

			tid = atoi(tid_dir->d_name);
			ret = cb(tid, buffer);
			if (ret < 0) {
				B_ERR("%s cb failed\n", __func__);
				break;
			}
			else if (ret == 1) {
				B_DEBUG("%s cb break\n", __func__);
				break;
			}
			else if (ret == 0) {
				B_DEBUG("%s cb success, and continue\n", __func__);
			}
		}
		closedir(task_dir);
		return ret;
	}

	B_ERR("%s: no find proc/%d/task\n", __func__, pid);
	return ret;
}

int aw_search_process(const char *procs)
{
	#define COMM_LEN 128
	int id = 0;
	int procs_pid = 0;
	int procs_start = 1000;
	int procs_end = 0xffff;
	int procs_target = 0;
	char procs_com[COMM_LEN];
	char comm[COMM_LEN];
	DIR* proc_dir;
	struct dirent *proc_entry;
	FILE *fp;

	if (!procs) {
		B_ERR("%s:input procs in null\n", __func__);
		return RETFAULT;
	}

	proc_dir = opendir("/proc");
	if (proc_dir == NULL) {
		B_ERR("%s:open proc failed\n", __func__);
		return RETFAULT;
	}
_search:
	while ((proc_entry = readdir(proc_dir)) != NULL) {
		id = atoi(proc_entry->d_name);
		if ((id != 0) && (id > procs_start) && (id < procs_end)) {
			snprintf(procs_com, sizeof(procs_com), "/proc/%d/comm", id);
			fp = fopen(procs_com, "r");
			if (fp) {
				fgets(comm, sizeof(comm), fp);
				fclose(fp);
				fp = NULL;
				if(!strncmp(comm, procs, strlen(procs))) { 
					B_INF("%s:get process %s\n",__func__, comm);
					procs_target = id;
					goto _exit;
				}
			}
		}
	}

	if ((procs_start < 0) && (!procs_pid)) {
		B_ERR("%s: not find proc\n", __func__);
		closedir(proc_dir);
		return RETFAULT;
	}

	if (!procs_pid) {
		B_ERR("%s: proc_id is invalid\n", __func__);
		procs_end = procs_start;
		procs_start -= 200;
		goto _search;
	}

_exit:
	closedir(proc_dir);
	return procs_target;
}

/*
return 0 : Get the target process
return -1: Not found the target process
*/
int aw_check_process(const char *procs, const char *name)
{
	FILE *fp;
	char comm[128];
	if (!procs | !name) {
		B_ERR("%s:input is null\n", __func__);
		return -1;
	}

	fp = fopen(procs, "r");
	if (fp) {
		fgets(comm, sizeof(comm), fp);
		fclose(fp);
		fp = NULL;
		if (!strncmp(comm, name, strlen(name))) {
			B_INF("%s:get process %s\n", __func__, comm);
			return 0;
		}
	}
	return -1;
}


int aw_inotify_event(const char *path, int file_flag)
{
	char event_buf[1024];
	struct inotify_event *event;
	int errno_t;
	int ifd = 0;
	int wfd = 0;

	if (!path) {
		B_ERR("%s:inpt path is null\n", __func__);
		return RETFAULT;
	}

	ifd = inotify_init();
	wfd = inotify_add_watch(ifd, path, file_flag);
	if (wfd < 0) {
		B_ERR("%s: inotify_add_watch failed, wfd=%d\n", __func__, wfd);
		return RETFAULT;
	}

	while (1) {
		int res = read(ifd, event_buf, sizeof(event_buf));
		errno_t = errno;
		if (res < (int)sizeof(*event)) {
			if(errno_t == EINTR)
				continue;
			break;
		}
		inotify_rm_watch(ifd, wfd);
		close(ifd);
		return 0;
	}
	inotify_rm_watch(ifd, wfd);
	close(ifd);
	return -1;
}

#ifdef SOCRE_USAGE
struct cpu_info {
    long unsigned utime, ntime, stime, itime;
    long unsigned iowtime, irqtime, sirqtime;
};
static struct cpu_info old_cpu, new_cpu;
#endif

static struct proc_info **old_procs, **new_procs;
static int num_old_procs, num_new_procs;
static struct proc_info *free_procs;
static int num_used_procs, num_free_procs;

static int aw_read_thread_name(char *filename, struct proc_info *proc) {
    FILE *file;
    char line[MAX_LINE];

    line[0] = '\0';
    file = fopen(filename, "r");
    if (!file) return 1;
    fgets(line, MAX_LINE, file);
    fclose(file);
    if (strlen(line) > 0) {
        strncpy(proc->tname, line, THREAD_NAME_LEN);
        proc->tname[THREAD_NAME_LEN-1] = 0;
    } else
        proc->tname[0] = 0;
    return 0;
}

static void add_proc(int proc_num, struct proc_info *proc) {
    int i;

    if (proc_num >= num_new_procs) {
        new_procs = (struct proc_info **)realloc(new_procs, 2 * num_new_procs * sizeof(struct proc_info *));
        if (!new_procs) B_ERR("Could not expand procs array.\n");
        for (i = num_new_procs; i < 2 * num_new_procs; i++)
            new_procs[i] = NULL;
        num_new_procs = 2 * num_new_procs;
    }
    new_procs[proc_num] = proc;
}

static struct proc_info *alloc_proc(void) {
    struct proc_info *proc;

    if (free_procs) {
        proc = free_procs;
        free_procs = free_procs->next;
        num_free_procs--;
    } else {
        proc = (struct proc_info *)malloc(sizeof(*proc));
        if (!proc) B_ERR("Could not allocate struct process_info.\n");
    }
    num_used_procs++;

    return proc;
}

static void free_proc(struct proc_info *proc) {
    proc->next = free_procs;
    free_procs = proc;

    num_used_procs--;
    num_free_procs++;
}

static void free_old_procs(void) {
    int i;

    for (i = 0; i < num_old_procs; i++)
        if (old_procs[i])
            free_proc(old_procs[i]);

    free(old_procs);
}

static struct proc_info *find_old_proc(pid_t pid, pid_t tid) {
    int i;

    for (i = 0; i < num_old_procs; i++)
        if (old_procs[i] && (old_procs[i]->pid == pid) && (old_procs[i]->tid == tid))
            return old_procs[i];

    return NULL;
}

static int numcmp(long long a, long long b) {
    if (a < b) return -1;
    if (a > b) return 1;
    return 0;
}

static int proc_cpu_cmp(const void *a, const void *b) {
    struct proc_info *pa, *pb;

    pa = *((struct proc_info **)a); pb = *((struct proc_info **)b);

    if (!pa && !pb) return 0;
    if (!pa) return 1;
    if (!pb) return -1;

    return -numcmp(pa->delta_time, pb->delta_time);
}

static void print_procs(void) {
    int i;
    struct proc_info *old_proc;

    for (i = 0; i < num_new_procs; i++) {
        if (new_procs[i]) {
            old_proc = find_old_proc(new_procs[i]->pid, new_procs[i]->tid);
            if (old_proc) {
                new_procs[i]->delta_utime = new_procs[i]->utime - old_proc->utime;
                new_procs[i]->delta_stime = new_procs[i]->stime - old_proc->stime;
            } else {
                new_procs[i]->delta_utime = 0;
                new_procs[i]->delta_stime = 0;
            }
            new_procs[i]->delta_time = new_procs[i]->delta_utime + new_procs[i]->delta_stime;
        }
    }
	
#ifdef SOCRE_USAGE
    long unsigned  total_delta_time = (new_cpu.utime + new_cpu.ntime + new_cpu.stime + new_cpu.itime
                        + new_cpu.iowtime + new_cpu.irqtime + new_cpu.sirqtime)
                     - (old_cpu.utime + old_cpu.ntime + old_cpu.stime + old_cpu.itime
                        + old_cpu.iowtime + old_cpu.irqtime + old_cpu.sirqtime);
#endif

    qsort(new_procs, num_new_procs, sizeof(struct proc_info *), proc_cpu_cmp);
	
	return;
}

static int set_priority(struct proc_info *proc, int prio) 
{
    int prio_old = getpriority(PRIO_PROCESS, proc->tid);
	B_DEBUG("attach process pid=%d, thread%s %d prio %d\n", proc->pid, proc->tname, proc->tid, prio_old);
	
	if (prio != prio_old) {
		if (setpriority(PRIO_PROCESS, proc->tid, prio) < 0) {
        	B_ERR("%s set pio=%d failed\n", proc->tname, prio);
			return -1;
		}
	}

	prio_old = getpriority(PRIO_PROCESS, proc->tid);
	B_DEBUG("attach thread%s %d change prio %d\n", proc->tname, proc->tid, prio_old);

    return 0;
}

static int read_stat(char *filename, struct proc_info *proc) {
    FILE *file;
    char buf[MAX_LINE], *open_paren, *close_paren;

    file = fopen(filename, "r");
    if (!file) return 1;
    fgets(buf, MAX_LINE, file);
    fclose(file);

    /* Split at first '(' and last ')' to get process name. */
    open_paren = strchr(buf, '(');
    close_paren = strrchr(buf, ')');
    if (!open_paren || !close_paren) return 1;

    *open_paren = *close_paren = '\0';
    strncpy(proc->tname, open_paren + 1, THREAD_NAME_LEN);
    proc->tname[THREAD_NAME_LEN-1] = 0;

    // Scan rest of string.
    long pr;
    sscanf(close_paren + 1,
           " %c "
           "%*d %*d %*d %*d %*d %*d %*d %*d %*d %*d "
           "%" SCNu64 // utime %lu (14)
           "%" SCNu64 // stime %lu (15)
           "%*d %*d "
           "%ld " // priority %ld (18)
           "%ld " // nice %ld (19)
           "%*d %*d %*d "
           "%" SCNu64 // vsize %lu (23)
           "%" SCNu64, // rss %ld (24)
           &proc->state,
           &proc->utime,
           &proc->stime,
           &pr,
           &proc->ni,
           &proc->vss,
           &proc->rss);

    // Translate the PR field.
    if (pr < -9) strcpy(proc->pr, "RT");
    else snprintf(proc->pr, sizeof(proc->pr), "%ld", pr);

    return 0;
}

static int aw_read_procs(struct proc_info *proc_target) {
    DIR *task_dir;
    struct dirent *tid_dir;
    char filename[64];
    
    int proc_num = 0;
    struct proc_info *proc;
	struct proc_info proc_temp;
    pid_t pid, tid;
	
	new_procs = (struct proc_info **)calloc(INIT_PROCS * THREAD_MULT, sizeof(struct proc_info *));
    num_new_procs = INIT_PROCS * THREAD_MULT;

#ifdef SOCRE_USAGE
	FILE *file;
    file = fopen("/proc/stat", "r");
    if (!file) die("Could not open /proc/stat.\n");
    fscanf(file, "cpu  %lu %lu %lu %lu %lu %lu %lu", &new_cpu.utime, &new_cpu.ntime, &new_cpu.stime,
            &new_cpu.itime, &new_cpu.iowtime, &new_cpu.irqtime, &new_cpu.sirqtime);
    fclose(file);
#endif

	pid = proc_target->pid;
    sprintf(filename, "/proc/%d/task", pid);
    task_dir = opendir(filename);
    if (task_dir) {
        while ((tid_dir = readdir(task_dir))) {
            if (!isdigit(tid_dir->d_name[0]))
                continue;

            tid = atoi(tid_dir->d_name);
			proc_temp.tid = tid;
			
            sprintf(filename, "/proc/%d/task/%d/comm", pid, tid);
            aw_read_thread_name(filename, &proc_temp);

			B_DEBUG("search thread name %s\n", proc_temp.tname);
			if (!strncmp(proc_temp.tname, proc_target->tname, strlen(proc_target->tname))) {
				B_DEBUG("get target thread name %s\n", proc_temp.tname);

				proc = alloc_proc();
				proc->pid = pid; 
				proc->tid = tid;		
			
				sprintf(filename, "/proc/%d/task/%d/stat", pid, tid);
				read_stat(filename, proc);
				add_proc(proc_num++, proc);
			}	
		}
		closedir(task_dir);
		B_DEBUG("search thread proc_num %d \n", proc_num);
		if (proc_num)
			return 0;		
	}
	return -1;
}

int aw_king_monitor_threads(pid_t pid)
{
	int ret = -1;
	int cpu_mask = 2;
	free_procs = NULL;
    num_new_procs = num_old_procs = 0;
    new_procs = old_procs = NULL;
	num_used_procs = num_free_procs = 0;

	struct proc_info proc_target;
	memset(&proc_target, 0, sizeof(proc_target));
	proc_target.pid = pid;
	sprintf(proc_target.tname, "UnityMain");
	B_DEBUG("target thread name %s\n", proc_target.tname);
	
    ret = aw_read_procs(&proc_target);
	if (ret < 0) 
		return ret;

    old_procs = new_procs;
    num_old_procs = num_new_procs;

#ifdef SOCRE_USAGE
    memcpy(&old_cpu, &new_cpu, sizeof(old_cpu));
#endif
	usleep(1000*1000);
    ret = aw_read_procs(&proc_target);
	if (!ret) {
		print_procs();
		set_priority((struct proc_info *)new_procs[0], -10);
		aw_search_threads(((struct proc_info *)new_procs[0])->pid, aw_taskset_threads, &cpu_mask);
		aw_taskset(((struct proc_info *)new_procs[0])->tid, 4);
		free_old_procs();
	} 

    return ret;
}





