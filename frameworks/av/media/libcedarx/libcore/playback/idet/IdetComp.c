#include "cdx_log.h"
#include <malloc.h>
#include <stdio.h>
#include <unistd.h>

#define  _GNU_SOURCE
#include <sched.h>
#include <sys/syscall.h>
#include <sys/prctl.h>
#include <sys/resource.h>
#include <cutils/log.h>
#include <cutils/properties.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <vdecoder.h>

#include "AW_idet.h"
#include "IdetComp.h"

#if THREAD_SET_CPU
//define for system call
#define CPU_SETSIZE 1024
typedef unsigned long __cpu_mask;
#define __NCPUBITS  (8 * sizeof (__cpu_mask))

#define __CPUELT(cpu) ((cpu) / __NCPUBITS)
#define __CPUMASK(cpu) ((__cpu_mask) 1 << ((cpu) % __NCPUBITS))

typedef struct
{
   __cpu_mask __bits[CPU_SETSIZE / __NCPUBITS];
} cpu_set_t;

#define CPU_SET(cpu, cpusetp) \
    ((cpusetp)->__bits[__CPUELT (cpu)] |= (1UL << ((cpu) % __NCPUBITS)))
#define CPU_ZERO(cpusetp) \
    memset((cpusetp), 0, sizeof(cpu_set_t))
#define CPU_ISSET(cpu, cpusetp) (((cpusetp)->__bits[__CPUELT (cpu)] & __CPUMASK (cpu)) != 0)
#endif

static void* IdetThread(void* arg);

//for makeSureType
static const char *type2str(idet_Type type)
{
    switch(type) {
        case TFF          : return "tff";
        case BFF          : return "bff";
        case PROGRESSIVE  : return "progressive";
        case UNDETERMINED : return "undetermined";
    }
    return NULL;
}

static int64_t systemTime()
{
    struct timespec t;
    t.tv_sec = t.tv_nsec = 0;
    clock_gettime(CLOCK_MONOTONIC, &t);
    return t.tv_sec*1000000000LL + t.tv_nsec;
}

IdetCompCtx*  IdetCompCreate(void)
{
    IdetCompCtx* pIdetComp;
    pIdetComp = (IdetCompCtx*)calloc(1,sizeof(*pIdetComp));
    if(pIdetComp == NULL)
    {
        loge("memory alloc fail.");
        return NULL;
    }
    //init
    IdetCompInit(pIdetComp);
    pIdetComp->mqIdetStart = AwMessageQueueCreate(MAX_NUM_OF_THREAD, "IdetStart");
    pIdetComp->mqIdetDone  = AwMessageQueueCreate(MAX_NUM_OF_THREAD, "IdetDone");

    if(pIdetComp->mqIdetStart == NULL || pIdetComp->mqIdetDone == NULL){
        loge("IdetComp create message queue fail !");
        free(pIdetComp);
        return NULL;
    }

    //create thread
    int i;
    for (i = 0; i < MAX_NUM_OF_THREAD; i++)
    {
        //create detector
        pIdetComp->pIdet[i] = detectorCreate();
        if(pIdetComp->pIdet[i] == NULL){
            loge("The IdetCtx is NULL");
            return NULL;
        }
        pIdetComp->mThreadContext[i].nIndex = i;
        pIdetComp->mThreadContext[i].pThread_idetComp = pIdetComp;
        if(pthread_create(&pIdetComp->mTid[i], NULL, IdetThread, (void*)&pIdetComp->mThreadContext[i]) == 0){
            pIdetComp->bThreadCreated[i] = 1;
            logd("thread %d create success ",pIdetComp->mThreadContext[i].nIndex);
        }
        else{
            pIdetComp->bThreadCreated[i] = 0;
            loge("thread %d create failed ",pIdetComp->mThreadContext[i].nIndex);
        }
    }
    return  pIdetComp;
}

int  IdetCompInit(IdetCompCtx* pIdetComp)
{
    int i;
    for(i = 0; i < MAX_NUM_OF_THREAD; i++)
    {
      //reset IdetCtx
      pIdetComp->pIdet[i] = NULL;
      pIdetComp->bThreadCreated[i] = 0;
      pIdetComp->bCalcuDone[i]     = 0;
    }
    pIdetComp->nNumberOfThread     = MAX_NUM_OF_THREAD;
    pIdetComp->pPrePicture         = NULL;
    pIdetComp->pCurPicture         = NULL;
    pIdetComp->pNextPicture        = NULL;
    pIdetComp->pTempPicture        = NULL;

    //make sure type init
    pIdetComp->nCount              = 0;
    pIdetComp->bIdetFlag           = 0;
    pIdetComp->interlacedFlag      = -1;
    pIdetComp->last_type           = UNDETERMINED;
    memset(pIdetComp->history, UNDETERMINED, HISTORY_SIZE);
    //*add for intrelaced change progressive
    pIdetComp->bIchangedP          = 0;

    return 0;
}

int IdetCompReset(IdetCompCtx* pIdetComp)
{
    pIdetComp->pTempPicture        = NULL;
    pIdetComp->nCount              = 0;
    pIdetComp->bIdetFlag           = 0;
    logd("IDET_TEST,IdetCompReset !");
    return 0;
}

int checkWorkDone(IdetCompCtx* pIdetComp)
{
    int work_done = 1;
    int i;
    for (i = 0; i < pIdetComp->nNumberOfThread; i++)
    {
        work_done = work_done && pIdetComp->bCalcuDone[i];
    }
    return work_done;
}

int makeSureType(IdetCompCtx* pIdetComp)
{
    idet_Type     type, best_type;
    RepeatedField repeat;
    int match = 0;//make sure best_type

    //define threshold
    float       interlace_threshold   = 1.04f;
    float       progressive_threshold = 1.5f;
    float       repeat_threshold      = 3.0f;

    int64_t     alpha0 = 0;
    int64_t     alpha1 = 0;
    int64_t     delta  = 0;
    int64_t     gamma0 = 0;
    int64_t     gamma1 = 0;
    int                  i;
    for(i = 0; i < pIdetComp->nNumberOfThread; i++)
    {
        //get threads calcu values and merge
        alpha0 += pIdetComp->mCalcuValue[i].alpha0;
        alpha1 += pIdetComp->mCalcuValue[i].alpha1;
        delta  += pIdetComp->mCalcuValue[i].delta;
        gamma0 += pIdetComp->mCalcuValue[i].gamma0;
        gamma1 += pIdetComp->mCalcuValue[i].gamma1;
    }

     if     (alpha0 > interlace_threshold * alpha1){
        type = TFF;
    }else if(alpha1 > interlace_threshold * alpha0){
        type = BFF;
    }else if(alpha1 > progressive_threshold * delta){
        type = PROGRESSIVE;
    }else{
        type = UNDETERMINED;
    }

    //This is the repeat judge
    if ( gamma0 > repeat_threshold * gamma1 ){
        repeat = REPEAT_TOP;
    } else if ( gamma1 > repeat_threshold * gamma0 ){
        repeat = REPEAT_BOTTOM;
    } else {
        repeat = REPEAT_NONE;
    }

    //To determine the best type
    memmove(pIdetComp->history+1, pIdetComp->history, HISTORY_SIZE - 1);
    pIdetComp->history[0] = type;
    best_type = UNDETERMINED;
    for(i=0; i<HISTORY_SIZE; i++){
        if(pIdetComp->history[i] != UNDETERMINED){
            if(best_type == UNDETERMINED)
                best_type = pIdetComp->history[i];
            if(pIdetComp->history[i] == best_type){
                match++;
            }else{
                match=0;
                memset(pIdetComp->history, UNDETERMINED, HISTORY_SIZE);//reset history
                break;
            }
        }
    }

    idet_Type  tmpType = pIdetComp->last_type;
    //make sure last type
    if(pIdetComp->last_type == UNDETERMINED){
        if(match>2) pIdetComp->last_type = best_type;
    }
    else{
        if(match>(HISTORY_SIZE - 1)) {
                //in detection process,type changes from interlaced to  progressive
            if(pIdetComp->last_type != PROGRESSIVE && best_type == PROGRESSIVE ){
                /*
                              if the original type is progressive,but  the detection result is interlaced in the process of interlaced detection,
                              we should change progressive to intrelaced firstly.And then ,even if the detection result is progressive , we
                              shloud not change last_type to progressive.
                              such as nanfanggouwu channel of iptv.
                          */
                if(pIdetComp->pCurPicture->bIsProgressive == 1){
                    logv("original type is progressive,interlaced appeared progressive,ignore !");
                    memset(pIdetComp->history, UNDETERMINED, HISTORY_SIZE);
                }

                /*
                              if the original type is interlaced,but  the detection result is progressive in the process of interlaced detection,
                              we should change intrelaced  to progressive firstly.And then, if the detection result is progressive , we shloud
                              also change last_type to progressive.
                              such as guangdongtiyu channel of iptv,program is interlaced,advertisement is progressive.
                          */
                else{
                    pIdetComp->last_type = best_type;
                    logv("original type is interlaced,interlaced appeared progressive,change !");
                }
            }
            else
                pIdetComp->last_type = best_type;
        }
    }
    //judge the last_type changed
    if( tmpType != UNDETERMINED && tmpType != pIdetComp->last_type){
        logd("IDET_TEST,dete_type changed , %12s changed %12s!",type2str(tmpType),type2str(pIdetComp->last_type));

        //*add for intrelaced change progressive
        if(tmpType != PROGRESSIVE && pIdetComp->last_type == PROGRESSIVE){
          logv("interlaced changed progressive !");
          pIdetComp->bIchangedP = 1;
        }
        //pIdetComp->nCount = 0;
    }

    //pIdetComp->total_type[type]++;
    //pIdetComp->total_last_type[pIdetComp->last_type]++;
    logv("IDET_TEST,Single frame:%12s   %d, Multi frame:%12s   %d \n" ,type2str(type),pIdetComp->total_type[type], \
        type2str(pIdetComp->last_type),pIdetComp->total_last_type[pIdetComp->last_type]);
    if(pIdetComp->last_type == PROGRESSIVE)
        pIdetComp->interlacedFlag = 0;
    else if(pIdetComp->last_type == UNDETERMINED){
        logv("UNDETERMINED Type !");
    }
    else{
         pIdetComp->interlacedFlag = 1;
         if(pIdetComp->last_type == TFF){
              if(pIdetComp->pCurPicture->bTopFieldFirst == 0)
                 pIdetComp->pCurPicture->bTopFieldFirst = 1;
         }
         else{
              if(pIdetComp->pCurPicture->bTopFieldFirst == 1)
                 pIdetComp->pCurPicture->bTopFieldFirst = 0;
         }
    }
    return 0;
}

void  IdetCompDestroy(IdetCompCtx* pIdetComp)
{
    AwMessage msg_start;
    int       i;
    for (i = 0; i < MAX_NUM_OF_THREAD; i++)
    {
        msg_start.messageId = MSG_IDET_EXIT;
        msg_start.int64Value = i;
        AwMessageQueuePostMessage(pIdetComp->mqIdetStart, &msg_start);
    }

    for (i = 0; i < MAX_NUM_OF_THREAD; i++)
    {
        pthread_join(pIdetComp->mTid[i], NULL);
    }
    detectorDestroy(pIdetComp);
    AwMessageQueueDestroy(pIdetComp->mqIdetStart);
    AwMessageQueueDestroy(pIdetComp->mqIdetDone);

    if(pIdetComp){
        logd("free pIdetComp !");
        free(pIdetComp);
    }
}

void detectorDestroy(IdetCompCtx* pIdetComp)
{
    int i;
    for(i = 0; i < MAX_NUM_OF_THREAD; i++)
    {
        if (pIdetComp->pIdet[i])
        {
            //logd("free idet,index  : %d ",i);
            free(pIdetComp->pIdet[i]);
        }
    }
}

//ToDo:
//add for frame skip detection
int IdetCompControlProcess(IdetCompCtx* pIdetComp)
{
    int nRate = 0;
    if(pIdetComp->pCurPicture->nFrameRate == 0)
        nRate = 25;
    else
        nRate= pIdetComp->pCurPicture->nFrameRate/1000;

    if((pIdetComp->nCount <= nRate * 20) && (pIdetComp->bIdetFlag == 0)){
            pIdetComp->nCount++;
            logd("start detect ,nFrameRate : %d!",nRate);
            IdetCompPostProcess(pIdetComp);
            if(pIdetComp->nCount == (nRate * 20 + 1)){
                pIdetComp->bIdetFlag = 1;
                logd("stop detect!");
            }
    }
    if( pIdetComp->bIdetFlag == 1){
            pIdetComp->nCount--;
            if(pIdetComp->nCount == nRate * 10){
                pIdetComp->bIdetFlag  = 0;
                pIdetComp->pTempPicture = NULL ;
                logd("restart detect!");
            }
    }
    return 0;
}

int IdetCompPostProcess(IdetCompCtx* pIdetComp)
{
    AwMessage msg_start;
    AwMessage msg_done;
    int              i;

    int64_t last_time = systemTime()/1000;
    if (pIdetComp->pCurPicture->nWidth > 640
        && pIdetComp->pCurPicture->nHeight > 480)
    {
        pIdetComp->nNumberOfThread = 4;
    }
    else
    {
        pIdetComp->nNumberOfThread = 2;
    }

    if (pIdetComp->nNumberOfThread > MAX_NUM_OF_THREAD)
    {
        pIdetComp->nNumberOfThread = MAX_NUM_OF_THREAD;
    }

    for (i = 0; i < pIdetComp->nNumberOfThread; i++){
            msg_start.messageId = MSG_IDET_START;
            msg_start.int64Value = i;
            AwMessageQueuePostMessage(pIdetComp->mqIdetStart, &msg_start);
            pIdetComp->bCalcuDone[i] = 0;
    }
    do{
        AwMessageQueueGetMessage(pIdetComp->mqIdetDone, &msg_done);
        pIdetComp->bCalcuDone[msg_done.int64Value]= 1;
    }while(!checkWorkDone(pIdetComp));
    makeSureType(pIdetComp);
#if DEBUG_CALCU_TIME
    pIdetComp->mFrameCount++;
    pIdetComp->mIdetTotalTime += systemTime()/1000 - last_time;
    logd("detect time avg is :%lld !",pIdetComp->mIdetTotalTime/pIdetComp->mFrameCount/1000);
#endif

    return 0;
}

static void* IdetThread(void* arg)
{
    Thread_Context*  pThreadContext;
    IdetCompCtx*     pIdetComp;
    AwMessage        msg_start;
    AwMessage        msg_done;

    pThreadContext     = (Thread_Context*)arg;
    pIdetComp          = pThreadContext->pThread_idetComp;
    int threadId       = pThreadContext->nIndex;
    int index          = 0;
    int ret_val        = 0;

#if THREAD_SET_CPU
    char name[64];
    snprintf(name, sizeof(name), "detect_thread_%d", threadId);
    //set the process/thread name
    prctl(PR_SET_NAME, name);

    int status = setpriority(PRIO_PROCESS, gettid(), -3);
    logd("detect_Thread setpriority return: %d", status);
    status = getpriority(PRIO_PROCESS, gettid());
    logd("detect_Thread getpriority: %d", status);
    cpu_set_t mask;
    cpu_set_t get;
    CPU_ZERO(&mask);
    //init cpu ID
    int cpu = (threadId % pIdetComp->nNumberOfThread);
    //current cpu ID join mask
    CPU_SET(cpu, &mask);

    //attch PID/TID to CPU
    if(syscall(__NR_sched_setaffinity, 0, sizeof(mask), &mask) == -1)
    {
        logd("set affinity failed, cpu: %d, threadId: %d, pIdetComp->nNumberOfThread: %d", cpu, threadId, pIdetComp->nNumberOfThread);
    }
    else
    {
        logd("set affinity ok, cpu: %d, threadId: %d, pIdetComp->nNumberOfThread: %d", cpu, threadId, pIdetComp->nNumberOfThread);
        CPU_ZERO(&get);
        //get CPU of PID/TID
        if (syscall(__NR_sched_getaffinity, 0, sizeof(get), &get) == -1)
        {
            logw("warning: could not get thread affinity, continuing...");
        }
        else
        {
            int i;
            for (i = 0; i < pIdetComp->nNumberOfThread; i++)
                {
                    if (CPU_ISSET(i, &get))
                    {
                        logd("thread %d is running on cpu %d", threadId,i);
                    }
                }
        }
    }

#endif

    while(1){
        if( AwMessageQueueGetMessage(pIdetComp->mqIdetStart, &msg_start) != 0){
            logw("invalid mq");
            continue;
        }
        else{
            index = msg_start.int64Value;
            if (msg_start.messageId == MSG_IDET_EXIT)
            {
                 logd("thread %d exit !",index);
                 break;
            }
            if (pIdetComp->pPrePicture == NULL || pIdetComp->pCurPicture == NULL || pIdetComp->pNextPicture == NULL){
                 logd("error picture pointer !");
                 //if have null pointer,not detect(also used in seek)
                 pIdetComp->pTempPicture = NULL;
                 ret_val = -1;
                 continue;
            }
            pIdetComp->pIdet[index]->nIndex          = index;
            pIdetComp->pIdet[index]->pPic[0]         = pIdetComp->pPrePicture;
            pIdetComp->pIdet[index]->pPic[1]         = pIdetComp->pCurPicture;
            pIdetComp->pIdet[index]->pPic[2]         = pIdetComp->pNextPicture;
            pIdetComp->pIdet[index]->nThread_number  = pIdetComp->nNumberOfThread;

            //calculate value
            pIdetComp->mCalcuValue[index] = doCalculate(pIdetComp->pIdet[index]);

            msg_done.messageId = MSG_IDET_DONE;
            msg_done.int64Value = index;
            AwMessageQueuePostMessage(pIdetComp->mqIdetDone, &msg_done);
        }
    }
    pthread_exit(&ret_val);
    return NULL;
}



