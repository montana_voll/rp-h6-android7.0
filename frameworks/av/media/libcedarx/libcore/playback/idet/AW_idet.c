#include <string.h>
#include <stdio.h>
#include <malloc.h>
#include "cdx_log.h"
#include "AW_idet.h"

#define BUILTIN_CONSTANT_P(x) 0
#define IDET_RSHIFT(a,b) (!BUILTIN_CONSTANT_P(b) ? -((-(a)) >> (b)) \
                                                       : ((a) + (1<<(b)) - 1) >> (b))
#define IDET_ABS(a) ((a) >= 0 ? (a) : (-(a)))

//define skip pixel number to reduce the time of algorithm
#define SKIP_PIX 16

int idet_filter_line(const uint8_t *a, const uint8_t *b, const uint8_t *c, int w ,int index)
{
    int x;
    int ret=0;
    a += index * w;
    b += index * w;
    c += index * w;
    for(x=index * w ; x<(index + 1)*w;){
        int v = (*a + *c) - 2 * *b;
        a += SKIP_PIX;
        b += SKIP_PIX;
        c += SKIP_PIX;
        x += SKIP_PIX;
        ret += IDET_ABS(v);
    }
    return ret;
}

IdetCtx*  detectorCreate(void)
{
    IdetCtx* pIdet;
    pIdet = (IdetCtx*)calloc(1,sizeof(*pIdet));
    if(pIdet == NULL)
    {
        loge("memory calloc fail !");
        return NULL;
    }
    //init
    if(detectorInit(pIdet) == 0)
        logd("detectorInit  ok !");
    else
        logd("detectorInit  failed !");

    return pIdet;
}

int  detectorInit(IdetCtx* pIdet)
{
        int i ;
        for(i = 0;i < 3;i++){
            pIdet->pPic[i] = NULL;
        }
        pIdet->nIndex           = 0;
        pIdet->filter_line      = idet_filter_line;

        return 0;
}

calcuValue doCalculate(IdetCtx* pIdet)
{
    int             y, i;
    int             nSizeY, nSizeUV, nSize;
    int             mIndex;
    int64_t         alpha[2] = {0};
    int64_t         delta    =   0;
    int64_t         gamma[2] = {0};

    //define  values of  AVPixFmtDescriptor(NV21/NV21)
    uint8_t         nb_components = 3;
    uint8_t         log2_chroma_w = 1;
    uint8_t         log2_chroma_h = 1;

    char *nData0 = NULL;
    char *nData1 = NULL;

    VideoPicture*  prePicture  =  pIdet->pPic[0];
    VideoPicture*  curPicture  =  pIdet->pPic[1];
    VideoPicture*  nextPicture =  pIdet->pPic[2];

    if(prePicture == curPicture){
        logd("new test !");
    }
    nSizeY     = curPicture->nWidth * curPicture->nHeight;
    nSizeUV    = nSizeY >> 2;
    mIndex     = pIdet->nIndex;//thread id

    //This loop is to calculate the values for  the first judge with pictures
    for (i = 0; i < nb_components; i++) {
            int           w = curPicture->nWidth;
            int           h = curPicture->nHeight;
            int  lineStride = curPicture->nLineStride;
            //int  linesize[3] = {lineStride,lineStride/2,lineStride/2};//Y.U.V SIZE of picture
            //int refs = linesize[i];
            int        refs = lineStride;//For video, size in bytes of each picture line;for audio, size in bytes of each plane.
            if (i && i<3) {
                //U and V  is half of Y
                w = IDET_RSHIFT(w, log2_chroma_w);
                h = IDET_RSHIFT(h, log2_chroma_h);
            }
            //thread process width
            int thread_w = w/pIdet->nThread_number;
            for (y = 2; y < h - 2; y++) {
                int k = 0;
                uint8_t *prev = NULL;
                uint8_t *cur  = NULL;
                uint8_t *next = NULL;
                if( i == 0){
                    prev =  (unsigned char *) &(prePicture->pData0[y*refs]);
                    cur  =  (unsigned char *) &(curPicture->pData0[y*refs]);
                    next =  (unsigned char *) &(nextPicture->pData0[y*refs]);
                }
                else if( i == 1){
                    prev =  (unsigned char *) &(prePicture->pData1[y*refs]);
                    cur  =  (unsigned char *) &(curPicture->pData1[y*refs]);
                    next =  (unsigned char *) &(nextPicture->pData1[y*refs]);
                }
                else{
                    prev =  (unsigned char *) &(((prePicture->pData1)+refs/2)[y*refs]);
                    cur  =  (unsigned char *) &(((curPicture->pData1)+refs/2)[y*refs]);
                    next =  (unsigned char *) &(((nextPicture->pData1)+refs/2)[y*refs]);
                }
                alpha[ y   &1] += pIdet->filter_line(cur-refs, prev, cur+refs, thread_w ,mIndex);
                alpha[(y^1)&1] += pIdet->filter_line(cur-refs, next, cur+refs, thread_w ,mIndex);
                delta          += pIdet->filter_line(cur-refs,  cur, cur+refs, thread_w ,mIndex);
                gamma[(y^1)&1] += pIdet->filter_line(cur     , prev, cur     , thread_w ,mIndex);
           }
        }
    //get the calculate value
    calcuValue    calcuResult = {
                .alpha0 = alpha[0],
                .alpha1 = alpha[1],
                .delta  =    delta,
                .gamma0 = gamma[0],
                .gamma1 = gamma[1],
    };
    return calcuResult;
}





