#ifndef IDETCOMP_H
#define IDETCOMP_H

#include <string.h>
#include <stdio.h>
#include "AwMessageQueue.h"
#include "baseComponent.h"
#include "AW_idet.h"

#define MAX_NUM_OF_THREAD    4
#define HISTORY_SIZE         25

#define THREAD_SET_CPU       0
#define DEBUG_CALCU_TIME     0

typedef enum {
    MSG_IDET_START,
    MSG_IDET_DONE,
    MSG_IDET_EXIT,
}IDET_MSG;

typedef enum {
    TFF,
    BFF,
    PROGRESSIVE,
    UNDETERMINED,
} idet_Type;

typedef enum {
    REPEAT_NONE,
    REPEAT_TOP,
    REPEAT_BOTTOM,
} RepeatedField;

typedef struct IdetCompCtx IdetCompCtx;

typedef struct {
    int                     nIndex;
    IdetCompCtx*            pThread_idetComp;
}Thread_Context;

struct IdetCompCtx{
    IdetCtx*                pIdet[MAX_NUM_OF_THREAD];
    pthread_t               mTid[MAX_NUM_OF_THREAD];
    Thread_Context          mThreadContext[MAX_NUM_OF_THREAD];

    AwMessageQueue*         mqIdetStart;
    AwMessageQueue*         mqIdetDone;

    int                     bThreadCreated[MAX_NUM_OF_THREAD];
    int                     nNumberOfThread;
    VideoPicture*           pPrePicture;
    VideoPicture*           pCurPicture;
    VideoPicture*           pNextPicture;
    VideoPicture*           pTempPicture;

    int                     bCalcuDone[MAX_NUM_OF_THREAD];
    //get calculate value
    calcuValue              mCalcuValue[MAX_NUM_OF_THREAD];

    //make sure type
    int                     nCount;
    int                     bIdetFlag;
    int                     total_type[4];
    int                     total_last_type[4];
    uint8_t                 history[HISTORY_SIZE];//make sure best_type
    idet_Type               last_type;
    int                     interlacedFlag;//interlaced Flag
    //add for intrelaced change progressive
    int                     bIchangedP;

#if DEBUG_CALCU_TIME
    int64_t                 mFrameCount;
    int64_t                 mIdetTotalTime;
#endif
};

IdetCompCtx*  IdetCompCreate(void);//create interlaced detection component
void IdetCompDestroy(IdetCompCtx* pIdetComp);//destroy
int  IdetCompInit(IdetCompCtx* pIdetComp);//init
int  IdetCompReset(IdetCompCtx* pIdetComp);//reset
int  IdetCompControlProcess(IdetCompCtx* pIdetComp);// control interlaced detection  process
int  IdetCompPostProcess(IdetCompCtx* pIdetComp);//post message to detect thread and wait the  thread works done
void detectorDestroy(IdetCompCtx* pIdetComp);//destroy detector
int  makeSureType(IdetCompCtx* pIdetComp);//make sure final field type
int  checkWorkDone(IdetCompCtx* pIdetComp);

#endif

